# In this verification process we assume that the no of days in a year are no fixed
#We approximate the no of people required to have the same birthday based on the no. of days specified by the user
from random import randint
import functools

no_of_days=input(" No of days in the year ")
#the days in the year are numbered 1 to the no. of days specified.
#Considering a dataset of 10,000 people
no_of_people    = [0 for x in range(0,10000)]
# For 10000 cases we consider the no. of people required to repeat a day 
# and then average out the length to provide an approximation.
for i in range(0,10000):
    birthday_list = []
    #since we need atleast 2 people , we start with a dummy.
    birthday_list.append(randint(1,no_of_days))
    rand_var = randint(1,no_of_days)
    while(rand_var not in birthday_list):
        birthday_list.append(rand_var)
        rand_var = randint(1,no_of_days)
    no_of_people[i] = len(birthday_list)+1
avg_no_of_people =functools.reduce(lambda x, y: x + y, no_of_people) / len(no_of_people)
print(avg_no_of_people)
