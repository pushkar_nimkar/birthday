from random import uniform
from matplotlib import pyplot as plt
import numpy as np
import sys

# something that makes us realize how slow python is
NUM_TEST_CASES=10000
result = np.zeros(365)

for n, p in enumerate(result):
    # the variable is little deviated from its original purpose
    average = 0
    # following lines are simply creating the progress bar
    progress = n*100/365
    sys.stdout.write('Progress [%s%s]\r' %('#'*progress, '-'*(100-progress)))
    sys.stdout.flush()

    # test for common birthdays
    for i in range(NUM_TEST_CASES):
        tmp = np.zeros(365, dtype=int)

        # produces random birthdays with uniform distribution
        for j in range(n):
            tmp[int(uniform(0, 365))] += 1
        # if there is more than one birthday on some day in tmp
        # then add one to average
        average += (sum(tmp > 1) > 0)

    # compute possibility of more than one birthday for n ppl
    p = float(average) / NUM_TEST_CASES
    result[n] = p

print '\n'
print result
plt.plot(result)
plt.show()

# end
